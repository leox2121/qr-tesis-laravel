<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Illuminate\Contracts\Auth\Guard;

class MDsadmin
{
    protected $auth;

    public function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
         if ($this->auth->user()->id_rol_usuario != 1) {
            return redirect()->to('home')->with(['message'=>'Sin privilegios de Super Administrador']);
        }
        return $next($request);
    }
}
