<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\UtilController;
use DB;

class AuthenticateController extends Controller
{
    private $util;

    function __construct()
    {
        $this->util = new UtilController();
    }

    public function Authenticate(Request $request)
    {
        $count = $this->verificacion($request);
        if ($count < 1) {
            return $this->util->Respuesta([], 'El usuario no cuenta con este rol.', 500);
        }
        $Csrf_Token =  \Illuminate\Support\Facades\Session::token();
        $credenciales = $request->only('email', 'password');
        $rol = $request->get('rol');
        $Authenticate = null;
        try {
            if (!$Authenticate = JWTAuth::attempt($credenciales)) {
                return response()->json(['error' => 'Su credencial es invalida']);
            }
        } catch (JWTException $ex) {
            return response()->json(['error' => $ex], 500);
        }
        $usuario = Auth::user();
        $response = [
            'Authenticate' => $Authenticate,
            'Csrf_Token' => $Csrf_Token,
            'usuario' => ['nombre' => $usuario->name, 'id' => $usuario->id, 'cedula' => $usuario->cedula, 'mensaje' => 'Bienvenidos a QrMed', 'rol' => $rol],
        ];
        return $response;
    }

    public function getRolUsuario(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'string|required'
            ]);
            $email = $request->get('email');
            $consulta = DB::table('rol_usuario as ru')
                ->join('users as u', 'ru.id_usuario', '=', 'u.id')
                ->join('rol as r', 'ru.id_rol', '=', 'r.id')->select('r.id', 'r.nombre', 'r.codigo')->where('u.email', '=', $email)->get();
            return $this->util->Respuesta($consulta, 'Se consulto correctamente.', 200);
        } catch (\Exception $ex) {
            return $this->util->Respuesta($ex, 'Algo a ocurrido ponerse en contadto con el administrador.', 500);
        }
    }

    public function verificacion($request)
    {
        $email = $request->get('email');
        $codigo = $request->get('rol');
        $consulta = DB::table('rol_usuario as ru')
            ->join('users as u', 'ru.id_usuario', '=', 'u.id')
            ->join('rol as r', 'ru.id_rol', '=', 'r.id')->select('r.id', 'r.nombre', 'r.codigo')
            ->where('u.email', '=', $email)->where('r.codigo', '=', $codigo)->get();
        return count($consulta);
    }
}
