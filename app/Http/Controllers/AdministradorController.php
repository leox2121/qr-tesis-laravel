<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UsuarioModel;

class AdministradorController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index()
    {
        $administrador = UsuarioModel::join('tb_rol as rol','users.id_rol_usuario','=','rol.id')->where('rol.descripcion','=','ADMINISTRADOR')->select('users.*','rol.descripcion as rol')->get();
        return view('sadmin.admin.index',compact('administrador'));
    }

    public function create()
    {
        return view('sadmin.admin.create');
    }

    public function store(Request $request)
    {
        $validate = $this->validate($request,[
            'name'=>'required'
        ]);
        	
        $UsuarioModel = new UsuarioModel();
        $UsuarioModel->name = $request->get('name');
        $UsuarioModel->id_rol_usuario = 2;
        $UsuarioModel->email = $request->get('email');
        $UsuarioModel->password = bcrypt($request->get('password'));
        $UsuarioModel->cedula = $request->get('cedula');
        $UsuarioModel->apellidos = $request->get('apellidos');
        $UsuarioModel->save();
        return redirect()->route('administrador.index')->with(['message'=>'se creo correctamente']);

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $administrador = UsuarioModel::find($id);
        return view('sadmin.admin.edit',compact('administrador'));
    }

    public function update(Request $request, $id)
    {
        $UsuarioModel = UsuarioModel::find($id);
        $UsuarioModel->name = $request->get('name');
        $UsuarioModel->id_rol_usuario = 2;
        $UsuarioModel->email = $request->get('email');
        $UsuarioModel->password = bcrypt($request->get('password'));
        $UsuarioModel->cedula = $request->get('cedula');
        $UsuarioModel->apellidos = $request->get('apellidos');
        $UsuarioModel->update();
        return redirect()->route('administrador.index');
    }

    public function eliminar($id)
    {
        $UsuarioModel = UsuarioModel::find($id);
        $UsuarioModel->delete();
        return redirect()->route('administrador.index');
    }
}
