<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolUsuarioModel extends Model
{
    protected $table = 'rol_usuario';
    protected $primaryKey='id';
    public $timestamps = false;
}
