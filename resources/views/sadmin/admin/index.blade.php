@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <a href="{{route('administrador.create')}}" class="btn btn-success">Nuevo Administrador</a>
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Administrador</div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                        <tr colspan="2" style="text-align:center;">
                        <th style="font-size:13px;color:#1F618D">ID</th>
                        <th style="font-size:13px;color:#1F618D">Rol</th>
                        <th style="font-size:13px;color:#1F618D">Nombre</th>
                        <th style="font-size:13px;color:#1F618D">Apellido</th>
                        <th style="font-size:13px;color:#1F618D">Cedula</th>
                        <th style="font-size:13px;color:#1F618D">Email</th>
                        <th style="font-size:13px;color:#1F618D">Opciones</th>
                       </tr>
                    </thead>
                    @foreach($administrador as $val)
                        <tr>
                            <td style="font-size:12px">{{$val->id}}</td>
                            <td style="font-size:12px">{{$val->rol}}</td>
                            <td style="font-size:12px">{{$val->name}}</td>
                            <td style="font-size:12px">{{$val->apellidos}}</td>
                            <td style="font-size:12px">{{$val->cedula}}</td>
                            <td style="font-size:12px">{{$val->email}}</td>
                            <td>
                                <a class="btn-xs btn-warning" href="{{route('administrador.edit',$val->id)}}">Editar</a>
                                <a class="btn-xs btn-danger" href="{{route('eliminaradmin',$val->id)}}">Eliminar</a>
                            </td>
                        </tr>
                    @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
