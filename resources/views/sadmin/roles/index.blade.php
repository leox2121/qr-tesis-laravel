@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <a href="{{route('formroles')}}" class="btn btn-success">Nuevo Rol</a>
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Roles</div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                        <tr colspan="2" style="text-align:center;">
                        <th style="font-size:13px;color:#1F618D">ID</th>
                        <th style="font-size:13px;color:#1F618D">Descrpcion</th>
                        <th style="font-size:13px;color:#1F618D">Accion</th>
                       </tr>
                    </thead>
                    @foreach($roles as $val)
                        <tr>
                            <td style="font-size:12px">{{$val->id}}</td>
                            <td style="font-size:12px">{{$val->descripcion}}</td>
                            <td>
                                <a class="btn-xs btn-warning" href="{{route('formrolesedit',$val->id)}}">Editar</a>
                                <a class="btn-xs btn-danger" href="{{route('eliminarroles',$val->id)}}">Eliminar</a>
                            </td>
                        </tr>
                    @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- prueba del git con git lab otra-->
@endsection
