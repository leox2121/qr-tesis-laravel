CREATE TABLE `tb_rol` (
`id` integer(10) NOT NULL AUTO_INCREMENT,
`descripcion` varchar(255) NULL,
PRIMARY KEY (`id`) 
);
CREATE TABLE `users` (
`id` integer(10) NOT NULL AUTO_INCREMENT,
`id_rol_usuario` integer(10) NULL,
`name` varchar(50) NULL,
`password` longtext NULL,
`email` varchar(100) NULL,
`created_at` datetime NULL,
`updated_at` datetime NULL,
`remember_token` longtext NULL,
`apellidos` varchar(255) NULL,
`cedula ` varchar(255) NULL,
PRIMARY KEY (`id`) 
);
CREATE TABLE `tb_restaurante` (
`id` integer(10) NOT NULL AUTO_INCREMENT,
`id_usuario` integer(10) NULL,
`nombre` varchar(255) NOT NULL,
`direccion` varchar(255) NULL,
PRIMARY KEY (`id`) 
);
CREATE TABLE `tb_plato` (
`id` integer(10) NOT NULL AUTO_INCREMENT,
`id_restaurante` integer(10) NULL,
`nombre_plato` varchar(255) NULL,
`valor_plato` double(10,0) NULL,
PRIMARY KEY (`id`) 
);

ALTER TABLE `users` ADD CONSTRAINT `fk_rol_usuario` FOREIGN KEY (`id_rol_usuario`) REFERENCES `tb_rol` (`id`);
ALTER TABLE `tb_plato` ADD CONSTRAINT `fk_plato_restaurante` FOREIGN KEY (`id_restaurante`) REFERENCES `tb_restaurante` (`id`);
ALTER TABLE `tb_restaurante` ADD CONSTRAINT `fk_estaurante_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`);

