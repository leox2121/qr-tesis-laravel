<?php


Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::post('consultaRol','AuthenticateController@getRolUsuario')->name('consultaRol');//getRolUsuario
Route::post('Authenticate', 'AuthenticateController@Authenticate')->name('Authenticate');

Route::group(['middleware' => 'jwt.auth'], function () {

	Route::post('usuario','ConsultasController@getUsuario');

	Route::post('categorias','ConsultasController@getCategoria');
	Route::post('productos','ConsultasController@getProducto');

	Route::post('ordenes','VentaController@getOrden');
	Route::post('ordenes/crear','VentaController@CrearOrden');
	Route::post('ordenes/eliminar','VentaController@EliminarOrden');
	
	Route::post('detalle','VentaController@getDetalle');
	Route::post('detalle/crear','VentaController@AgregarDetalle');
	Route::post('detalle/eliminar','VentaController@EliminarDetalle');
	Route::post('venta/finalizar','VentaController@FinalizarVenta');
	
	Route::post('cliente/crear','ClienteController@CrearCliente');
	Route::post('cliente/listar','ClienteController@getCliente');//getCliente

	Route::post('ingreso/crear','ingresoAreaController@postIngreso');//getConsultaIngreso
	Route::post('ingreso/consulta','ingresoAreaController@getConsultaIngreso');//getConsultaIngreso
	Route::post('ingreso/consultaPersonal','ingresoAreaController@getConsultaPersonal');//getConsultaPersonal
	

});



